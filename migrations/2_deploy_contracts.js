'use strict';

const OrderManager = artifacts.require("OrderManager");
const BuzzToken = artifacts.require("BuzzToken");

const fs = require("fs");
const path = require("path");

const writeToFile = async (entry) => {
    await fs.writeFileSync(
        path.join(__dirname, "..", "build", "addressRegistry.json"),
        JSON.stringify(entry)
    );
};

module.exports = function (deployer) {
    deployer.deploy(BuzzToken)
        .then(() => BuzzToken.deployed())
        .then(() => deployer.deploy(OrderManager, BuzzToken.address))
        .then(() => {
            console.log('BuzzToken', BuzzToken.address);
            console.log('OrderManager', OrderManager.address);

            return writeToFile({
                buzzToken: BuzzToken.address,
                orderManager: OrderManager.address
            });
        })
};