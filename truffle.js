'use strict';

module.exports = {
  networks: {
      development: {
        host: "127.0.0.1",
        port: 22000,
        network_id: "*",
        gasPrice: 0,
        gas: 3000000,
        // Supply chain address
        from: '0x69fb4070891ebd3986e771b41fbdecfe422d1496'
      }
  }
};
