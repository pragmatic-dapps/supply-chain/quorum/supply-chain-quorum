function prepareTestAccounts() {
	function registerAccounts() {
		var dropchain = personal.newAccount(String(0));

		console.log(dropchain);

		var accs = [];

		for(var i = 1; i <= 10; i++) {
			var acc = personal.newAccount(String(i));

			accs.push(acc);
		}

		console.log(accs);
	}

	function unlockAccounts() {
		for(var i = 0; i <= 10; i++) {
			var acc = web3.eth.accounts[i];

			personal.unlockAccount(acc, String(i), 1000000);
		}
	}

	registerAccounts();
  unlockAccounts();
}
