pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";

// Crowd sale token contract
// (c) by Pragmatic DLT
contract BuzzToken is ERC20, Ownable {
    using SafeMath for uint;

    string public name;
    string public symbol;
    uint public decimals;
    uint public totalSupply;
    uint public startDate;
    uint public bonusEnds;
    uint public endDate;

    // Amount of tokens per 1 ETH
    uint public exchangeRate;
    uint public bonusExchangeRate;

    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) approved;

    constructor() public {
        symbol = "TST";
        name = "Test ERC20 token";
        decimals = 18;
        bonusEnds = now + 1 weeks;
        endDate = now + 7 weeks;
        exchangeRate = 1000;
        bonusExchangeRate = 1200;

        // Supply chain address
        balances[0x69fb4070891ebd3986e771b41fbdecfe422d1496] = 10000 * 10 ** uint(decimals);

        // Brands addresses
        balances[0xf40a47d9d94f05ca69627400dbc1ae40fd95aaad] = 10000 * 10 ** uint(decimals);
        balances[0xd811262e154230fb88b62a9174535382d76b453c] = 10000 * 10 ** uint(decimals);
        balances[0x70d059d1aca6ba11f6daec23698a91cfde87bfe4] = 10000 * 10 ** uint(decimals);
        balances[0x7904d6c4a1f69abe356fd6fce2f8d14e4650bfa9] = 10000 * 10 ** uint(decimals);
        balances[0xab1e48ceed212216c278ef70d9ad5980b1f087f7] = 10000 * 10 ** uint(decimals);
        balances[0x84ac989c2c6e9bd1ba346f68fe0d94d599171816] = 10000 * 10 ** uint(decimals);
        balances[0xeda9efaf47446de2d1ddd09764cfb9bf2f638a5c] = 10000 * 10 ** uint(decimals);
        balances[0x3f9d0465e7bc8ae59fd7b4298f9edbeeb078f806] = 10000 * 10 ** uint(decimals);
        balances[0x9f6ec296185c1a5becd8c05b31146a1fc47c2b64] = 10000 * 10 ** uint(decimals);
        balances[0x11610983b9f291068b498d63f173466e060c2a50] = 10000 * 10 ** uint(decimals);
    }

    function totalSupply() public view returns (uint) {
        return totalSupply - balances[address(0)];
    }

    function balanceOf(address _tokenOwner) public view returns (uint) {
        return balances[_tokenOwner];
    }

    // Returns the amount which _spender is still allowed to withdraw from _owner
    function allowance(address _owner, address _spender) public view returns (uint) {
        return approved[_owner][_spender];
    }

    // Transfers _value amount of tokens to address _to, and fires the Transfer event.
    // If the _from account balance does not have enough tokens to spend - revert.
    // Transfers of 0 values are treated as normal transfers and the Transfer event is fired.
    function transfer(address _to, uint _value) public returns (bool) {
        require(balances[msg.sender] >= _value);
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);

        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    // Transfers _value amount of tokens from address _from to address _to and fires the Transfer event.
    // Transfers of 0 values are treated as normal transfers and the Transfer event is fired.
    // The calling account must already have sufficient tokens approved
    function transferFrom(address _from, address _to, uint _value) public returns (bool) {
        require(balances[_from] >= _value);
        require(approved[_from][msg.sender] >= _value);

        balances[_from] = balances[_from].sub(_value);
        approved[_from][msg.sender] = approved[_from][msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);

        emit Transfer(_from, _to, _value);
        return true;
    }

    // Allows _spender to withdraw multiple times, up to the _value amount.
    // If this function is called again it overwrites the current allowance with _value.
    function approve(address _spender, uint _value) public returns (bool) {
        approved[msg.sender][_spender] = _value;

        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function() public payable {
        require(now >= startDate && now <= endDate);
        require(msg.value >= 1 finney);

        uint tokens;

        if (now <= bonusEnds) {
            tokens = msg.value.mul(exchangeRate);
        } else {
            tokens = msg.value.mul(bonusExchangeRate);
        }

        balances[msg.sender] = balances[msg.sender].add(tokens);
        totalSupply = totalSupply.add(tokens);

        emit Transfer(address(0), msg.sender, tokens);
        owner.transfer(msg.value);
    }

    // Token owner can approve for _spender to 'transferFrom' tokens from the token owner's account.
    // The `spender` contract function `receiveApproval` is then executed
    function approveAndCall(address _spender, uint _value, bytes _data) public returns (bool) {
        approved[msg.sender][_spender] = _value;

        emit Approval(msg.sender, _spender, _value);
        ApproveAndCallFallBack(_spender).receiveApproval(msg.sender, _value, this, _data);
        return true;
    }

    // Owner can transfer out any accidentally sent ERC20 tokens
    function transferAnyERC20Token(address _tokenAddress, uint _value) public returns (bool) {
        return ERC20(_tokenAddress).transfer(owner, _value);
    }
}

// Contract function to receive approval and execute function in one call
contract ApproveAndCallFallBack {
    function receiveApproval(address _from, uint256 _value, address _tokenAddress, bytes _data) public;
}
