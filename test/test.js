const Web3 = require('web3');
const Promise = require('bluebird');
const fs = require('fs');
const path = require('path');

const addressRegistry = require('../build/addressRegistry');
const orderManager = require('../build/contracts/OrderManager.json');
const buzzToken = require('../build/contracts/BuzzToken.json');

const web3 = new Web3('http://localhost:22000');

let supplyChain = '0x69fb4070891ebd3986e771b41fbdecfe422d1496';
const TEST_PARTICIPANTS = [
  'brand-1',
  'brand-2',
  'brand-3',
  'brand-4',
  'brand-5',
  'brand-6',
  'brand-7',
  'brand-8',
  'brand-9',
  'brand-10',
];

let accounts = [
  '0xf40a47d9d94f05ca69627400dbc1ae40fd95aaad',
  '0xd811262e154230fb88b62a9174535382d76b453c',
  '0x70d059d1aca6ba11f6daec23698a91cfde87bfe4',
  '0x7904d6c4a1f69abe356fd6fce2f8d14e4650bfa9',
  '0xab1e48ceed212216c278ef70d9ad5980b1f087f7',
  '0x84ac989c2c6e9bd1ba346f68fe0d94d599171816',
  '0xeda9efaf47446de2d1ddd09764cfb9bf2f638a5c',
  '0x3f9d0465e7bc8ae59fd7b4298f9edbeeb078f806',
  '0x9f6ec296185c1a5becd8c05b31146a1fc47c2b64',
  '0x11610983b9f291068b498d63f173466e060c2a50',
];

const CONTRACT = new web3.eth.Contract(orderManager.abi,
    addressRegistry.orderManager);
const WALLET = new web3.eth.Contract(buzzToken.abi, addressRegistry.buzzToken);

const registerBrands = () => {
  const createBrand = () => {
    return TEST_PARTICIPANTS.map((p, index) => {
      CONTRACT.methods.createBrand(accounts[index], p).send({from: supplyChain});
    });
  };

  const approveBuzz = () => {
    return TEST_PARTICIPANTS.map(
        (p, index) => WALLET.methods.approve(addressRegistry.orderManager,
            999000000000000000000).send({from: accounts[index]}));
  };

  console.log('Registering brands...');

  return Promise.all(createBrand()).
      then(() => Promise.all(approveBuzz()).
          then(() => console.log('Brands registered.')));
};

const writeToFile = async (entry) => {
  await fs.appendFileSync(
      path.join(__dirname, 'test-results.csv'),
      entry + ',\r\n',
  );
};

const prepareTransactions = txCount => {
  const transactions = [];

  TEST_PARTICIPANTS.forEach((p, index) => {
    for (let i = 1; i <= txCount; i++) {
      const before = Date.now();
      transactions.push(
          CONTRACT.methods.createUnit(`Test unit #${i}`).
              send({from: accounts[index], gas: 200000}).
              then(() => Date.now() - before).
              catch(() => ''),
      );
    }
  });

  return transactions;
};

const saveResults = (resultInMillis, resp) => {
  console.log(resultInMillis);

  writeToFile(resp);
};

const execute = txCount => {
  const before = Date.now();

  return Promise.all(prepareTransactions(txCount)).then(resp => {
    const resultInMillis = Date.now() - before;

    return saveResults(resultInMillis, resp);
  });
};

const intervalTest = () => {
  console.log('intervalTest');

  const txList = [];

  return new Promise((resolve, reject) => {
    const job = setInterval(() => txList.push(execute(1)), 1000);
    setTimeout(() => {
      clearInterval(job);
      Promise.all(txList).then(() => resolve).catch(() => reject);
    }, 30000);
  });
};

const subsequentTest = async () => {
  console.log('subsequentTest');

  const sequence = [1, 3, 5, 7, 10, 15];

  for (let i = 0; i <= sequence.length - 1; i++) {
    await execute(sequence[i]);
  }
};

const test = async () => {
  await registerBrands();
  await subsequentTest();
  await intervalTest();
};

test().then(() => console.log('Finished.'));
